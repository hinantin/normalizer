xfst -f normalizer.xfst

xfst -f normalizer-relax.xfst

xfst -f spanish.xfst

xfst -f spanish-relax.xfst

xfst -f guesser.xfst

rm -f transducer.tar.gz

tar jcvf transducer.tar.gz normalizer.xfst normalizer-relax.xfst spanish.xfst spanish-relax.xfst guesser.xfst

# looking for a string 
# the result contains the line number
# grep -in "{paq}" /mnt/storage/hex/projects/clsquoia/squoia-rcastro/normalizer/normalizer/*.regex
