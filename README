Execute compile.sh




  ########################################################
  #                                                      #
  # SQUOIA XFST ANALYZER FOR CUZCO AND AYACUCHO QUECHUA  #
  #                                                      #
  ########################################################

IMPORTANT: please report failures, errors or wrong/missing analyses to arios@ifi.uzh.ch, so we can improve this tool!
------------------------------------------------------------------------------------------------------------------------

You need to have xfst installed on your system in order to compile this analyzer. 
Beware to install the right version for your system (32bit vs 64bit), as the with the wrong version, non-ASCII characters might in the transducers might behave unexpectedly!

Available from:
http://www.stanford.edu/~laurik/.book2software/

Supposing xfst is installed on your system, compile the package with either

$ xfst -l quechua-web.xfst

or 

$xfst -l quechua-web-db.xfst

The only difference between those two is that quechua-web-db,xfst will group morphemes into groups by separating them with a special boundary marker, see below.

You can also compile the transducers from the xfst shell with 'source':

xfst[0]: source quechua-web.xfst

or

xfst[0]: source quechua-web-db.xfst

Note that compilation takes a while, as those are large transducers.
If you have already compiled the analyzer, you can load the binary on the stack with (*.fst files=binaries) :

xfst[0]: load stack quechua-web.fst

or

xfst[0]: load stack quechua-web-db.fst

In order to analyze a word from the xfst shell, use 'apply up', or just short 'up' (here with quechua-web.fst):

xfst[1]: up rikhuwasharqanku
rikhuri[VRoot][=aparecer,ver][--]wa[VPers][+1.Obj][--]sha[Asp][+Prog][--]rqa[Tns][+NPst][--]nku[VPers][+3.Pl.Subj]

As mentioned before, the only difference between quechua-web and quechua-web-db is that the latter inserts boundary markers between morpheme groups ([^DB]). We need those, as we use those morpheme groups as basic units in our treebank:

xfst[1]: up rikhuwasharqanku
rikhuri[VRoot][=aparecer,ver][^DB][--]wa[VPers][+1.Obj][^DB][--]sha[Asp][+Prog][--]rqa[Tns][+NPst][--]nku[VPers][+3.Pl.Subj]

The analyzer can also be invoked directly from the shell with 'lookup'. You can also analyze files with this utility, not only single words. The input needs to be tokenized though (one word per line, punctuation marks separated from the words on their own line). 

$ echo 'wasinta' | lookup -flags 

The easier way is to use the Makefile, it takes normal text as input (doesn't need to be tokenized). The targets:

analysed.txt: analyses the text with quechua-web, analysed.txt then contains one word per line with the corresponding analysis

analysed-db.txt: same as analysed.txt, but with morpheme groups boundaries

analysed-db-truecase.txt: same as analysed-db.txt, but additionally outputs the original input word, useful if original casing should be preserved

vertical.tsv: verticalized output, one morpheme per line, words a separated by an empty line

failures.txt: prints a list of all words that quechua-web failed to analyze in a text

failures-db.txt: prints a list of all words that quechua-web-db failed to analyze in a text

clean: removes all binaries from the directory
