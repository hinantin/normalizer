############################################################
# targets:                                                 #
#							   #
# make analysed.txt ->  analysis with standard xfst output #
# make analysed.tsv ->  analysis with verticalized output  #
#			(1 morpheme per line)              #
# make failures.txt ->  produces a list of all words that  #
#			could not be analysed              #
# -db- targets: 	same as above, but inserts an      #
#			additional boundary marker [^DB]   #
#			to lump morphemes into groups      #
############################################################

# indicate path to text file to analyse:


TEXTS:=  /home/clsquoia/corpus/all_ocrCorr_qu/all.txt

normal:  normalizer.fst normalizer-relax.fst spanish.fst spanish-relax.fst
guess: guesser.fst


analysed.txt: normal
	cat $(TEXTS) | perl tokenize.pl | lookup -f lookup_guess.script -flags cKv29TT  | tail -n+2  > $@


failures.txt: normal
	cat $(TEXTS) | perl tokenize.pl |lookup -f lookup_guess.script -flags cKv29TT  | perl listwrongspellings.pl  > $@
	

analysed_noguess.txt: normal
	cat $(TEXTS) | perl tokenize.pl | lookup -f lookup_noguess.script -flags cKv29TT  | tail -n+2  > $@


failures_noguess.txt: normal
	cat $(TEXTS) | perl tokenize.pl | lookup -f lookup_noguess.script -flags cKv29TT  | perl listwrongspellings.pl  > $@
	
	
# clean: 
# 	rm  *.fst
# 
# %.fst:%.xfst
# 	xfst -f $<

